var vm_form = new Vue({
	el: '#container',
	data: {
		searchData: {
			ClientName: "",
			OutStoreID: "",
			BeginBillDate: "",
			EndBillDate: "",
			WHID: "",
			BizType: ""
		},
		param: {
			ClientName: {
				Column: "ClientName",
				Oper: 6, //参考枚举 EnumFilterOper
				OrderNo: 3,
				ParaName: "ClientName"
			},
			BizType: {
				Column: "BizType",
				Oper: 0,
				OrderNo: 4,
				ParaName: "BizType"
			},
			OutStoreID: {
				Column: "OutStoreID",
				Oper: 0,
				OrderNo: 5,
				ParaName: "OutStoreID"
			},
			BeginBillDate: {
				Column: "BillDate",
				Oper: 5,
				OrderNo: 7,
				ParaName: "BeginBillDate"
			},
			EndBillDate: {
				Column: "BillDate",
				Oper: 3,
				OrderNo: 8,
				ParaName: "EndBillDate"
			},
			WHID: {
				Column: "WHID",
				Oper: 0, //参考枚举 EnumFilterOper
				OrderNo: 6,
				ParaName: "WHID"
			}
		},
		authorization: "",
		param2: {
			OrgID: 0,
			PageIndex: 0,
			PageSize: 10000000,
			Paras: [],
			TopCount: 1,
			UserName: "Testser",
			WithCache: true
		},
		resultData: []
	},
	methods: {
		search: function() {
			var realSearchData = [];
			for(var item in this.searchData) {
				if(this.searchData[item] != "") {
					this.param[item]._Value = this.searchData[item];
					realSearchData.push(this.param[item]);
				}
			}
			sessionStorage.setItem("saleSearchParm", JSON.stringify(realSearchData));
			location.href = "sale-list.html?type=2";
		},
		getAuthorization: function() {
			this.authorization = "orgid=" + localStorage.getItem("OrgID") + "&user=" + localStorage.getItem("UserName") + "&token=" + localStorage.getItem("AccessToken");
		},
		getWH: function() {
			this.paramConstruct();
			this.getAuthorization();
			axios.post(server + '/MyCRMService/WareHouse/ModelList', JSON.stringify(this.param2), {
				emulateJSON: true,
				headers: {
					'Content-Type': 'application/json',
					'Authorization': this.authorization
				}

			}).then(
				function(res) {
					for(var i = 0; i < res.data.DataExt.length; i++) {
						var data = {};
						data.value = res.data.DataExt[i].WHID;
						data.text = res.data.DataExt[i].WHName;
						vm_form.resultData.push(data);
					}
					setElement(vm_form.resultData, "coupons", "coupons_value");
				},
				function(res) {});
		},
		paramConstruct: function() {
			this.param2.OrgID = localStorage.getItem("OrgID");
			this.param2.UserName = localStorage.getItem("UserName");
		},
		init: function() {
			this.searchData.BeginBillDate = this.getDate();
			this.searchData.EndBillDate = this.getDate();
		},
		getDate: function() {
			var date = new Date();
			var seperator1 = "-";
			var year = date.getFullYear();
			var month = date.getMonth() + 1;
			var strDate = date.getDate();
			if(month >= 1 && month <= 9) {
				month = "0" + month;
			}
			if(strDate >= 0 && strDate <= 9) {
				strDate = "0" + strDate;
			}
			var currentdate = year + seperator1 + month + seperator1 + strDate;
			return currentdate;
		}
	},
	created: function() {
		this.init();
		this.getWH();
	}
});

function setElement(data, id, valueId) {
	document.getElementById(id).addEventListener('tap', function() {
		var picker = new mui.PopPicker();
		picker.setData(data);
		picker.show(function(items) {
			$("#" + id).val(items[0].text);
			$("#" + valueId).val(items[0].value);
			vm_form.searchData.WHID = items[0].value;

		});
	}, false);
}

document.getElementById("remarks").addEventListener('tap', function() {
	var picker = new mui.PopPicker();
	picker.setData([{
		value: '6',
		text: '销售出库'
	}, {
		value: '7',
		text: '零售出库'
	}, {
		value: '8',
		text: '销售退货'
	}]);
	picker.show(function(items) {
		$("#remarks").val(items[0].text);
		$("#remarks_value").val(items[0].value);
		vm_form.searchData.BizType = items[0].value;
	});
}, false);
