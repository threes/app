var vm_form = new Vue({
	el: '#pullrefresh',
	data: {
		datas: {},
		param: {
			OrgID: 1,
			PageIndex: 0,
			PageSize: 2,
			UserName: "",
			WithCache: false,
		},
		inited: false,
		responseData: [],
		authorization: "",
		pulldownRefresh: false
	},
	methods: {
		getData: function() {
			this.paramConstruct();
			this.getAuthorization();
			axios.post(server + '/MyCRMService/OutStore/PageModelList', JSON.stringify(this.param), {
				emulateJSON: true,
				headers: {
					'Content-Type': 'application/json',
					'Authorization': this.authorization
				}
			}).then(
				function(res) {
					if(res.data.DataExt && res.data.DataExt.Items && res.data.DataExt.Items.length > 0) {
						vm_form.responseData = vm_form.responseData.concat(res.data.DataExt.Items);
						mui('#pullrefresh').pullRefresh().endPullupToRefresh(false);
					} else
						mui('#pullrefresh').pullRefresh().endPullupToRefresh(true);
				},
				function(res) {});
		},
		init: function() {
			if(!this.inited) {
				this.param.Paras = localStorage.getItem("saleSearchParm");
			}
		},
		getAuthorization: function() {
			this.authorization = "orgid=" + localStorage.getItem("OrgID") + "&user=" + localStorage.getItem("UserName") + "&token=" + localStorage.getItem("AccessToken");
		},
		paramConstruct: function() {
			this.param.OrgID = localStorage.getItem("OrgID");
			this.param.PageIndex = ++this.param.PageIndex;
			this.param.UserName = localStorage.getItem("UserName");
			if(sessionStorage.getItem("saleSearchParm"))
				this.param.Paras = eval('(' + sessionStorage.getItem("saleSearchParm") + ')');
			else
				this.param.Paras = [];
			console.info(this.param);
		},

		//日期转换
		ChangeDateFormat: function(val) {
			if(val != null) {
				var date = new Date(parseInt(val.replace("/Date(", "").replace(")/", ""), 10));
				//月份为0-11，所以+1，月份小于10时补个0
				var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
				var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
				/*var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
				var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();*/
				return date.getFullYear() + "-" + month + "-" + currentDate
			}
			return "";
		},
		GoDetail: function(val) {
			if(!window.localStorage) {
				alert("浏览器不支持！");
			} else {

                axios.get(server + '/MyCRMService/OutStore/'+this.param.OrgID+'/'+this.responseData[val].OutStoreID+'/'+this.param.UserName+'/True'
                ,{
                    emulateJSON: true,
                        headers: {
                    	'Content-Type': 'application/json',
                        'Authorization': this.authorization
                		}
                }).then(function (response) {
                    console.log(response.data.DataExt)	;
                	localStorage.setItem("detail", JSON.stringify(response.data.DataExt));
                	parent.location.href = "sale-detail.html";
                    })
                    .catch(function (error) {
                        console.log(error);
                    });

				//localStorage.setItem("detail", JSON.stringify(this.responseData[val]));
				//parent.location.href = "sale-detail.html";
			}
		},
		getHandName: function(val) {
			var strs = val.split(",");
			return strs[1];
		},
		getServiceType: function(numbers) {
			switch(numbers)
			{
				case 6:
					return "销售出库";
				case 7:
					return "零售出库";
				case 8:
					return "销售退货";
			}
		}

	},
	created: function() {

	}
});
var muis = mui.init({
	pullRefresh: {
		container: '#pullrefresh',
		/*down: {
			contentdown: "下拉可以刷新", //可选，在下拉可刷新状态时，下拉刷新控件上显示的标题内容

			contentover: "释放立即刷新", //可选，在释放可刷新状态时，下拉刷新控件上显示的标题内容

			contentrefresh: "正在刷新...", //可选，正在刷新状态时，下拉刷新控件上显示的标题内容

			callback: pulldownRefresh
		},*/
		up: {
			contentrefresh: '正在加载...',
			callback: pullupRefresh
		}
	}
});
/**
 * 下拉刷新具体业务实现
 */
function pulldownRefresh() {
	vm_form.getData();
	setTimeout(function() {
		mui('#pullrefresh').pullRefresh().endPulldownToRefresh(); //refresh completed
	}, 1500);

}
/**
 * 上拉加载具体业务实现
 */
function pullupRefresh() {
	vm_form.getData();
	setTimeout(function() {}, 1500);
}
if(mui.os.plus) {
	mui.plusReady(function() {

		mui('#pullrefresh').pullRefresh().pullupLoading();
	});
} else {
	mui.ready(function() {
		mui('#pullrefresh').pullRefresh().pullupLoading();
	});
}

mui('.mui-table-view').on('tap', '.my-order-item', function() {
	// 点击事件
	//location.href="sale-detail.html";
	// 暂时禁止滚动
	mui('#pullrefresh').pullRefresh().setStopped(true);
})　　　

mui('body').on('tap', '.mui-backdrop', function() {
	// 取消暂时禁止滚动
	mui('#pullrefresh').pullRefresh().setStopped(false);
})
mui('body').on('tap', '.mui-dtpicker-header', function(e) {
	var target = e.target;
	if(target.tagName === 'BUTTON') {
		// 取消暂时禁止滚动
		mui('#pullrefresh').pullRefresh().setStopped(false);
	}
})

mui('body').on('tap', '.my-order-wait-bt', function(e) {
	console.log(this.parent);
	//console.log(this.param);
	//console.log(e);
	//parent.location.href = "sale-detail.html";
}) /**/
