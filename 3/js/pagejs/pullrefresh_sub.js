mui.init({
	pullRefresh: {
		container: '#pullrefresh',
		down: {
			callback: pulldownRefresh
		},
		up: {
			contentrefresh: '正在加载...',
			callback: pullupRefresh
		}
	}
});
/**
 * 下拉刷新具体业务实现
 */
function pulldownRefresh() {
	setTimeout(function() {
		$("#content_body").prepend(" <li> <div class=\"my-order-item-ws\">" +
			"<div class=\"my-order-item-tit clearfix\" >" +
			"	<span>单据编号：888888888</span>" +
			"  <span class=\"my-order-wait-ws\">未审</span>" +
			"<span class=\"my-order-wait-bt\" onclick=\"detail()\">查看详情</span>" +
			" </div>" +
			"<div class=\"my-order-item-txt\">" +
			"	<div class=\"content_line\">" +
			"		<label class=\"left_name\">业务类型：</label>" +
			"	<label class=\"right_value\">销售</label>" +
			"	<div style=\"clear: both;\"></div>" +
			"</div>" +
			"<div class=\"content_line\">" +
			"	<label class=\"left_name\">销售客户：</label>" +
			"	<label class=\"right_value\">深圳海信科</label>" +
			"	<div style=\"clear: both;\"></div>" +
			"</div>" +
			"<div class=\"content_line\">" +
			"	<label class=\"left_name\">仓库：</label>" +
			"<label class=\"right_value\">赛格</label>" +
			"<div style=\"clear: both;\"></div>" +
			"</div>" +
			"<div class=\"content_line\">" +
			"		<label class=\"left_name\">时间：</label>" +
			"	<label class=\"right_value\">2018-07-13  13:30</label>" +
			"	<div style=\"clear: both;\"></div>" +
			"	</div>" +
			"	<div class=\"content_line\">" +
			"		<label class=\"left_name\">经手人：</label>" +
			"	<label class=\"right_value\">刘德华</label>" +
			"	<div style=\"clear: both;\"></div>" +
			"</div>" +
			"</div>" +
			" <div class=\"my-order-item-btn\">" +
			" <div class=\"pos-r\">" +
			"    	应收金额：￥2,400.00" +
			"  </div>  " +
			" </div>" +
			"</div></li>");
		mui('#pullrefresh').pullRefresh().endPulldownToRefresh(); //refresh completed
	}, 1500);
}
var count = 0;
/**
 * 上拉加载具体业务实现
 */
function pullupRefresh() {
	setTimeout(function() {
		$("#content_body").append("  <div class=\"my-order-item-ws\">" +
			"<div class=\"my-order-item-tit clearfix\" >" +
			"	<span>单据编号：888888888</span>" +
			"<span class=\"my-order-wait-ws\">未审</span>" +
			"<span class=\"my-order-wait-bt\" onclick=\"detail()\">查看详情</span>" +
			" </div>" +
			"<div class=\"my-order-item-txt\">" +
			"	<div class=\"content_line\">" +
			"		<label class=\"left_name\">业务类型：</label>" +
			"	<label class=\"right_value\">销售</label>" +
			"	<div style=\"clear: both;\"></div>" +
			"</div>" +
			"<div class=\"content_line\">" +
			"	<label class=\"left_name\">销售客户：</label>" +
			"	<label class=\"right_value\">深圳海信科</label>" +
			"	<div style=\"clear: both;\"></div>" +
			"</div>" +
			"<div class=\"content_line\">" +
			"	<label class=\"left_name\">仓库：</label>" +
			"<label class=\"right_value\">赛格</label>" +
			"<div style=\"clear: both;\"></div>" +
			"</div>" +
			"<div class=\"content_line\">" +
			"		<label class=\"left_name\">时间：</label>" +
			"	<label class=\"right_value\">2018-07-13  13:30</label>" +
			"	<div style=\"clear: both;\"></div>" +
			"	</div>" +
			"	<div class=\"content_line\">" +
			"		<label class=\"left_name\">经手人：</label>" +
			"	<label class=\"right_value\">刘德华</label>" +
			"	<div style=\"clear: both;\"></div>" +
			"</div>" +
			"</div>" +
			" <div class=\"my-order-item-btn\">" +
			" <div class=\"pos-r\">" +
			"    	应收金额：￥2,400.00" +
			"  </div>  " +
			" </div>" +
			"</div>");
		mui('#pullrefresh').pullRefresh().endPullupToRefresh((++count > 2));
	}, 1500);
}
if(mui.os.plus) {
	mui.plusReady(function() {
		setTimeout(function() {
			mui('#pullrefresh').pullRefresh().pullupLoading();
		}, 1000);
	});
} else {
	mui.ready(function() {
		mui('#pullrefresh').pullRefresh().pullupLoading();
	});
}

mui('.mui-table-view').on('tap', '.my-order-item', function() {
	// 点击事件
	//location.href="sale-detail.html";
	// 暂时禁止滚动
	mui('#pullrefresh').pullRefresh().setStopped(true);
})　　　

mui('body').on('tap', '.mui-backdrop', function() {
	// 取消暂时禁止滚动
	mui('#pullrefresh').pullRefresh().setStopped(false);
})
mui('body').on('tap', '.mui-dtpicker-header', function(e) {
	var target = e.target;
	if(target.tagName === 'BUTTON') {
		// 取消暂时禁止滚动
		mui('#pullrefresh').pullRefresh().setStopped(false);
	}
})

mui('body').on('tap', '.my-order-wait-bt', function(e) {
	//parent.location.href="sale-detail.html";
})
