var server = "http://122.114.63.193:7077";
// 添加响应拦截器
axios.interceptors.response.use(function (response) {
	if(response.statusText=="Unauthorized")
		top.location.href="./login.html";
	return response;
  }, function (error) {
  	if (error.response) {
            switch (error.response.status) {
                case 401:
                    location.href="/login";
            }
        }
	return Promise.reject(error);
  });
