var vm_form = new Vue({
	el: '#saleDetail',
	data: {
		name: 222,
		detail: "",
		count:0,
		sumPrice:0
	},
	methods: {
		//日期转换
		ChangeDateFormat: function(val) {
			if(val != null) {
				var date = new Date(parseInt(val.replace("/Date(", "").replace(")/", ""), 10));
				//月份为0-11，所以+1，月份小于10时补个0
				var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
				var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
				/*var hours = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
				var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();*/
				return date.getFullYear() + "-" + month + "-" + currentDate;
			}
			return "";
		},
		getHandName: function(val) {
			var strs = val.split(",");
			return strs[1];
		},
		fmoney: function(s, n) {
			if(s != '' & s != null) {
				n = n > 0 && n <= 20 ? n : 2;
				s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + "";
				var l = s.split(".")[0].split("").reverse(),
					r = s.split(".")[1];
				t = "";
				for(i = 0; i < l.length; i++) {
					t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");
				}
				return t.split("").reverse().join("") + "." + r;
			} else {
				return 0;
			}

		},
		init:function(){
			this.detail = JSON.parse(localStorage.getItem("detail"));
			for(var i =0;i<this.detail.Details.length;i++){
				this.count += this.detail.Details[i].Quantity;
				this.sumPrice += this.detail.Details[i].Amount;
			}
		}
	},
	created: function() {
		this.init();
	}

});
